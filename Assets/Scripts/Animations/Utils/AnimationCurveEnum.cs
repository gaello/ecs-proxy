﻿// Source: https://www.patrykgalach.com/2019/07/11/ecs-animations-in-unity/

/// <summary>
/// Animation curve enum available in Mathfx and MathfxECS class.
/// </summary>
public enum AnimationCurveEnum
{
    Hermite,
    Sinerp,
    Coserp,
    Berp,
    Bounce,
    Lerp,
    Clerp
}
