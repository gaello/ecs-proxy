﻿// Source: https://www.patrykgalach.com/2019/07/11/ecs-animations-in-unity/
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

/// <summary>
/// ECS system for Hover Tween
/// </summary>
public class HoverSystem : ComponentSystem
{
    /// <summary>
    /// Unity ECS Update called each frame.
    /// </summary>
    protected override void OnUpdate()
    {
        Entities.ForEach((Entity entity, ref Hover hover, ref Translation translation) =>
        {
            hover.ElapsedTime += Time.deltaTime;

            var t = math.clamp((hover.ElapsedTime - hover.Delay) / hover.Duration, 0.0f, 1.0f);
            translation.Value = MathfxHelper.CurvedValueECS(hover.AnimationCurve, hover.BottomValue, hover.TopValue, (-math.abs(t - 0.5f) + 0.5f) * 2);

            if (hover.ElapsedTime >= hover.Delay + hover.Duration)
            {
                if (hover.LoopAnimation)
                {
                    hover.ElapsedTime -= hover.Duration;
                }
                else
                {
                    translation.Value = hover.TopValue;
                    PostUpdateCommands.RemoveComponent<Hover>(entity);
                }
            }
        });
    }
}

