﻿// Source: https://www.patrykgalach.com/2019/07/11/ecs-animations-in-unity/
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

/// <summary>
/// ECS component for Hover Tween.
/// </summary>
[System.Serializable]
public struct Hover : IComponentData
{
    // Animation Baseline
    [ReadOnly] public float Delay; // Animation Delay.
    [ReadOnly] public float Duration; // Animation Duration.
    [ReadOnly] public bool LoopAnimation; // Is animation looping?
    [ReadOnly] public AnimationCurveEnum AnimationCurve; // Animation curve used for this tween.

    public float ElapsedTime; // Animation Elapsed Time.

    // Hover
    [ReadOnly] public float3 TopValue;
    [ReadOnly] public float3 BottomValue;
}