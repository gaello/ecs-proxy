﻿using Unity.Entities;
using UnityEngine;

/// <summary>
/// Proxy for Hover component.
/// </summary>
[DisallowMultipleComponent]
public class HoverProxy : ComponentDataProxy<Hover> { }